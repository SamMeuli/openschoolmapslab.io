ifdef::hoeflichkeitsform[]
Noch Fragen? Sehen Sie auch "Kontakt" auf OpenSchoolMaps!
endif::[]

ifndef::hoeflichkeitsform[]
Noch Fragen? Siehe "Kontakt" auf OpenSchoolMaps!
endif::[]
