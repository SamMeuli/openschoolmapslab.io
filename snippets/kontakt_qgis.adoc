ifdef::hoeflichkeitsform[]
Noch Fragen? Wenden Sie sich an die https://www.qgis.org/de/site/forusers/support.html[QGIS-Community]!
endif::[]

ifndef::hoeflichkeitsform[]
Noch Fragen? Wende dich an die https://www.qgis.org/de/site/forusers/support.html[QGIS-Community]!
endif::[]
