= Geokodieren von Adressen
OpenSchoolMaps.ch -- Freie Lernmaterialien zu freien Geodaten und Karten
:xrefstyle: short
:imagesdir: ../../../bilder/
:experimental:
include::../../../snippets/lang/de.adoc[]
include::../../../snippets/suppress_title_page.adoc[]

*Ein Arbeitsblatt für Selbstlerner, Studierende, Schülerinnen und Schüler sowie Lehrpersonen auf Sekundarstufe II (Sekundarschulen und Gymnasien).*
ifdef::show_solutions[- *LÖSUNGEN*]

Autor Fabian Germann, MSE Seminar GIS Location Intelligence, Frühjahrsemester 2023


== Einleitung

Für die maschinelle Verarbeitung von Geoinformationen werden meist Koordinaten verwendet.
So kann ein Computer z.B. die Distanz zwischen zwei Punkten präzise berechnen.
Wir Menschen hingegen können in der Regel mit Koordinaten wenig anfangen und bevorzugen eine menschenlesbare Darstellung wie Ortsbeschreibungen oder Adressen.

Unter Geokodieren (Englisch: geocoding) versteht man den Transformationsprozess von menschenlesbaren Namen in Koordinaten.
Handelt es sich um postalische Adressen, spricht man von Adressdaten geokodieren.

Geokodierung kommt täglich zum Einsatz, z.B. dann, wenn du den Namen eines Restaurants im Navi eingibst.
Darüber hinaus tritt Geokodierung häufig an der Schnittstelle zwischen Mensch und Maschine auf.
Aus diesen Gründen gehört Geokodierung zu den wichtigsten Technologien bei der Verarbeitung von Geodaten.

=== Ziele

Die Ziele dieses Arbeitsblattes sind:

* Du verstehst, wie die Geokodierung von Adressen grundsätzlich funktioniert.
* Du kannst in QGIS einzelne Adressen mittels dem Geokoder Nominatim geokodieren.
* Du kannst die wichtigsten Anwendungsfälle von Geokodierung aufzählen und du weisst, dass Geokodierung nicht immer die Lösung ist, obwohl es auf den ersten Blick so scheint.
* Du kannst in QGIS mehrere Adressen in einer CSV-Datei mittels der Funktion Nominatim-Sammelgeokodierung geokodieren.
* Du kannst in QGIS (GeoCoding Plugin) Koordinaten mittels «reverse geocoding» in Adressen transformieren.

=== Zeitplanung

_Zeitaufwand_: Diese Übung dauert ca. 30 Minuten.

=== Voraussetzungen

* _Software-/Hardware-Voraussetzungen_: Als Software wird https://download.qgis.org/[QGIS 3] (Version 3.20 oder neuer) vorausgesetzt, damit Nominatim nativ in QGIS verfügbar ist. Dieses Unterrichtsmaterial wurde mit der neusten herunterladbaren QGIS Version und mit dem neusten QGIS Long Term Release (LTR) getestet. QGIS läuft auf den gängigen Betriebssystemen Windows, MacOS und Linux.
* _Vorbereitungen_: QGIS ist installiert. Weitere Instruktionen siehe unten.
* _Eingangskompetenzen_: Es wird vorausgesetzt, dass Grundkenntnisse im Umgang mit einem Betriebssystem sowie Grundkenntnisse in QGIS 3 vorhanden sind.

=== Vorbereitungen

_Daten_: Adressliste (CSV-Datei) ist vorhanden. Die Datei kann  auf der Webseite von OpenSchoolMaps unter https://openschoolmaps.ch/pages/materialien.html#weitere-arbeitsblaetter-zu-qgis-3-und-gis[Unterrichtsmaterialien] heruntergeladen werden kann.

_QGIS Plugins_: https://plugins.qgis.org/plugins/quick_map_services/[QuickMapServices] sowie GeoCoding (für «reverse geocoding» in Aufgabe 3)

== Was ist Geokodieren?

Beim Geokodieren werden menschenlesbare Namen in geografische Koordinaten umgewandelt.
Ein paar Beispiele für solche Namen sind:

* Restaurant Alpenblick Meilen
* Naturhistorisches Museum Bern
* Genfergasse 14, 3011 Bern
* ETH Zürich
* Oberseestrasse 99 Rapperswil-Jona

Die oben aufgeführten Namen haben eines gemeinsam: Sie bezeichnen ein Objekt, das sich an einem bestimmten Standort befindet.
Das Ziel des Geokodierens ist es, die Koordinaten dieser Objekte anhand der Namen zu ermitteln.
Häufig handelt es sich bei den Namen um Adressen.
Der Fokus dieses Arbeitsblatts und der Übungsaufgaben ist deshalb das Geokodieren von Adressen.
Um Adressen auf einer Karte zu finden, wird eine enstprechende Software (Geocoder) benötigt.

<<abbildung-IO-geocoder>> visualisiert die Eingabe und Ausgabe eines Geocoders.
Beispielsweise nimmt der Geocoder den Text "Genfergasse 14, 3011 Bern" und gibt die Koordinaten (Breiten- und Längengrad) dafür aus.
Viele Geocoder bieten ebenfalls eine Umkehrfunktion an.
Dabei bilden die Koordinaten die Eingabe und werden in die nächstgelegene Adresse transformiert.

.Ein-/Ausgabe eines Geocoders für "Geocoding" bzw. "Reverse Geocoding"
[#abbildung-IO-geocoder]
image::geokodieren_von_adressen/swisscom-addr-to-lat-lon.png[]

== Wie funktioniert ein Geocoder?

Die grundsätzliche Funktionsweise eines Geocoders lässt sich wie folgt zusammenfassen:
In einer Datenbank sind verschiedene Informationen zu allen durchsuchbaren Orten gespeichert.
Für jeden Ort sind ausserdem die Koordinaten bekannt.
Algorithmen überführen dann die unstrukturierte Eingabe in ein strukturiertes Datenformat und suchen in der Datenbank nach passenden Orten.

Geocoder haben typischerweise zwei Eingabe-Optionen: Strukurierte und unstrukturierte Eingabe.
Bei der strukturierten Eingabe sind für jedes Attribute, wie Strassen, Ort, ein eigener Parameter vorgesehen.
Bei der unstrukturierten Eingabe muss man sich nicht an ein festes Schema halten.
Statt "Genfergasse 14, 3011 Bern" kann man z.B. auch die Postleitzahl weglassen und nach "Genfergasse 14 Bern" suchen.
Ausserdem sind verscheidene Schreibweisen, wie z. B. "Oberseestrasse 99" und "Oberseestr. 99" unterstützt.
Grundsätzlich kann nicht immer garantiert werden, dass ein perfekt passender Ort gefunden wird.
Deshalb gibt ein Geocoder häufig mehrere Orte aus, die nach der Relevanz absteigend sortiert sind.

Die konkrete Funktionsweise und der genaue Funktionsumfang des Geocoders sowie die Qualität der Resultate sind letztendlich vom verwendeten Geocoder abhängig.
Es gibt viele verschiedene Geocoder.
Sehr häufig wird das Geokodieren auch als Service angeboten.
Dies macht Geokodierung einfach verfügbar und hat den Vorteil, dass man die Geodaten in der Datenbank nicht selbst pflegen muss.
Zu den bekanntesten Services gehören beispielsweise:

* https://developers.google.com/maps/documentation/javascript/geocoding?hl=de[Google Geocoding-Dienst] (kommerziell, mit eigenen Daten)
* https://photon.komoot.io/[Photon] (Open Source und auf OpenStreetMap basierend)
* https://nominatim.org/[Nominatim] (Open Source und auf OpenStreetMap basierend)

Doch Geokodierung als Service hat auch Einschränkungn, die man beachten muss.
Die Dienstleistungen haben z.B. häufig einschränkende Nutzungsbedingungen.
Bei Nominatim ist beispielsweise nur eine Anfrage pro Sekunde möglich.
Bei kommerziellen Services sollte man auf jeden Fall das Preismodell genauer betrachten.

[NOTE]
.Hinweis für Power User
====
Müssen häufig viele Adressen geokodiert werden (z. B. mehrere tausend pro Tag), lohnt es sich möglicherweise einen eigenen Geocoder zu betreiben.
Die Open-Source-Geocoder bieten da den Vorteil, dass diese frei zugänglich sind und lokal installiert werden können.
====

== Aufgabe 1 -- Geokodieren mit Nominatim in QGIS

In dieser Aufgabe geht es darum, eine einzelne Adresse in QGIS zu geokodieren.
Der https://nominatim.org/[Nominatim Geocoder] ist in QGIS integriert.
Dieser Dienst verwendet Daten von https://www.openstreetmap.org/[OpenStreetMap].
In wenigen Schritten kannst du damit eine beliebige Eingabe geokodieren.

. Öffne QGIS und wähle `Web` > `QuickMapServices` > `OSM` > `OSM Standard`, um die Standardkarte von OSM einzufügen.
. Verwende das Textfeld in der Statusleiste, um Nominatim zu benützen. Die Eingabe muss mit einem `>` beginnen: z. B. `> Restaurant Alpenblick Meilen`
. Kann Nominatim alle Beispiele aus dem Abschnitt «Was ist geokodieren?» korrekt geokodieren? Verwende eigene Eingaben und versuche fehlerhafte Resultate zu erzeugen.

[NOTE]
.Verbesserung von Nominatim
====
Unter https://osm-place-search-logger.infs.ch/[osm-place-search-logger.infs.ch] können Resultate von Nominatim als korrekt gekennzeichnet werden. Diese Rückmeldung wird dann genutzt, um den Dienst weiter zu verbessern.
====

== Für welche Anwendungsfälle eignet sich Geokodierung?

Der Hauptanwendungsfall von Geokodieren ist die Standortsuche von Objekten anhand deren Namen/Adressen.
Diese geografische Suchfunktion kommt in zahlreichen spezifischen Anwendungsfällen zum Einsatz.
Ein paar Beispiele davon sind:

* Fahrtrouten optimieren
* Adressen auf Karten visualisieren
* Standort bezogenes Marketing
* Erstellung von Geostatistiken
* Argumented Reality Gaming

Doch nicht jedes Suchproblem lässt sich mit Geokodieren lösen.
Möchte man z.B. nach allen italienischen Restaurants in Zürich suchen, ist ein Geocoder nicht geeignet.
Bei solchen Suchen nach Typen von Ojekten erwartet man schliesslich mehrere treffende Resultate.
Von einem Geocoder hingegen erwartet man das bestmögliche Resultat zur Eingabe.

Geokodieren ist besonders hilfreich, wenn es darum geht, eine unstrukturierte Texteingabe von Menschen zu verarbeiten.
Liegen z. B. Adressen in einem strikten Schema vor, ist der direkte Abgleich mit den Adressdaten der Schweizerischen Post wohl möglich die bessere Lösung.

== Aufgabe 2 -- Adressliste in QGIS auf Karte visualisieren

In dieser Aufgabe verwendest du die Nominatim-Sammelgeokodierungs-Funktion, um mehrere Adressen in einer Datei zu geokodieren.

. Öffne QGIS und wähle `Web` > `QuickMapServices` > `OSM` > `OSM Standard` um die Standard-OSM-Karte eizufügen, sofern du dies nicht bereits getan hast.
. Ziehe die Datei `address_list.csv` mit der Maus in QGIS hinein.
. Suche im Fenster `Verarbeitungswerkzeuge` nach `Nominatim` und wähle `Nominatim-Sammelgeokodierung` aus.
+
.Verarbeitungswerkzeuge in QGIS
image::geokodieren_von_adressen/verarbeitungswerkzeuge_in_qgis.png[pdfwidth=50%]

. Wähle anschliessend als Eingabelayer `address_list` und als Adressfeld `Address` aus.
+
.Nominatim-Sammelgeokodierung
image::geokodieren_von_adressen/nominatim-sammelgeokodierung-window.png[pdfwidth=70%]

. Starte den Prozess. Überprüfe einzelne Adressen z. B. mit Google Maps. Kann Nominatim alle Adressen korrekt geokodieren?

== Aufgabe 3 -- «Reverse Geocoding» in QGIS

In dieser Aufgabe wirst du das GeoCoding-Plugin in QGIS verwenden, um mittels «reverse geocoding» die nächstgelegene Adresse eines beliebigen Punktes zu ermitteln.

. Öffne QGIS und wähle `Web` > `QuickMapServices` > `OSM` > `OSM Standard` um die Standard OSM Karte einzufügen, sofern du dies nicht bereits getan hast.
. Wähle `Erweiterungen` > `GeoCoding` > `ReverseGeoCoding`.
+
.Erweiterung GeoCoding
image::geokodieren_von_adressen/reverse_geocoding_menu_item.png[pdfwidth=50%]

. Mit dem ausgewählten Werkzeug kannst du irgendwo auf die Karte klicken. Es wird dann ein Punkt mit der nächstgelegenen Adresse als Label erzeugt.

[NOTE]
.Batch Reverse Geocoding
====
Das https://plugins.qgis.org/plugins/mmqgis/[Plugin MMQGIS] unterstützt Reverse Geocoding für mehrere Koordinaten (Stapelverarbeitung).
====

== Testfragen

Sind die folgenden Aussagen richtig oder falsch?

. Beim Geokodieren (Englisch: geocoding) werden Koordinaten in Adressen umgewandelt.
. Internetdienste für das Geokodieren bieten unter anderem den Vorteil, dass die Geodaten in der Datenbank nicht selbst aktualisiert werden müssen.
. Um sämtliche Campingplätze im Tessin zu suchen, ist ein Geocoder das richtige Suchinstrument.
. Beim Geokodieren einer Adresse kann es vorkommen, dass mehrere Orte bzw. deren Koordinaten als mögliche Resultate in Frage kommen.
. Beim Geokodieren muss die Eingabe einem strikten Schema entsprechen.
. Der in QGIS integrierte Geocoder Nominatim verwendet Daten von OpenStreetMap.
. Obwohl Nominatim ein open source Geocoder ist, kann dieser nicht lokal betrieben werden.
. Nominatim ist ein Internetdienst für das Geokodieren, der keine Nutzungslimiten hat. Damit können problemlos tausende Adressen innert wenigen Sekunden geokodiert werden.


ifdef::show_solutions[]
====
.Lösungen

. falsch: Es wird das «reverse geocoding» beschrieben.
. richtig
. falsch: Es werden mehrere treffende Resultate erwartet. Dafür ist ein Geocoder nicht geignet.
. richtig
. falsch: Ein Geocoder kann mit unstrukturierter Texteingabe umgehen.
. richtig
. falsch: Nominatim kann lokal betrieben werden.
. falsch: Nominatim verarbeitet nur ein Anfrage pro Sekunde.
====
endif::show_solutions[]

== Schlussfolgerung und Ausblick

Dieses Arbeitsblatt behandelt den Einstieg in das Thema Geokodieren von Adressen in QGIS.
Du besitzt nun die nötigen Grundkenntnisse, um das Thema weiter zu vertiefen.
Ausserdem kannst du in deinen zukünftigen GIS-Projekten einzelne sowie mehrere Adressen aus einer Datei geokodieren.

== Weiterführende Quellen

Weiterführende Quellen sind:

* https://media.ccc.de/v/fossgis2023-23895-geocoding-fr-einsteiger#t=854[FOSSGIS Berlin: Vortrag «Geocoding für Einsteiger» Sarah Hoffmann]
* https://docs.qgis.org/3.28/en/docs/user_manual/processing_algs/qgis/vectorgeneral.html#batch-nominatim-geocoder[Dokumentation QGIS: Batch Nominatim Geocoder]

---

include::../../../snippets/kontakt_qgis.adoc[]

include::../../../snippets/license.adoc[]
