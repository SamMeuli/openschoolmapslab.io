= Erstellen von Ausschnitten aus Rasterbildern mit QGIS ("`QGIS Raster Cutter`" Plugin)
OpenSchoolMaps.ch -- Freie Lernmaterialien zu freien Geodaten und Karten
:xrefstyle: short
:imagesdir: ../../../bilder/erstellen_von_ausschnitten_aus_rasterbildern_mit_qgis/
:experimental:
include::../../../snippets/lang/de.adoc[]
include::../../../snippets/suppress_title_page.adoc[]
:sectnums:
:sectnumlevels: 1

Dies ist ein Tutorial wie man mit dem QGIS-Desktop-Programm (Open Source) aus einem grossen Raster-Bild (bzw. einem Kartenservice oder einer lokalen Datei)
einen Ausschnitt lokal zur Weiterverarbeitung ausschneiden (englisch ‘to cut’) und speichern kann.
Zudem ist es eine Dokumentation des QGIS-Plugins ‘Raster Cutter’.

_Autoren: Stefan Keller und Fabio Zahner, beide Geometa Lab OST - sowie viele weitere Rezensenten.
Juni 2022 Dokumenten-Lizenz CC0 1.0 Fragen und Rückmeldungen an mailto:stefan.keller@ost.ch[Stefan Keller]._

TIP: Wenn sie an Vektordaten interessiert sind, dann gibt es dazu folgendes Tutorial
"`https://openschoolmaps.ch/pages/materialien.html#vektordaten-ausschneiden[Erstellen von Ausschnitten aus Vektordaten mit QGIS ('`QGIS Vector Cutting`')]`"

==  Einführung

Viele kennen Luftbilder zum Beispiel von Swisstopo oder von kantonalen GIS-Portalen.
Diese Bilder enthalten eine Fülle von Informationen, die eigene lokale Analysen ermöglichen.
Und immer mehr Behördendaten - sogenannte Open Government Data (OGD) - mit Themen über Grund und Boden werden offen und frei verfügbar.

Genau genommen ist ein Luftbild ein sogenanntes '`Ortophoto`', d.h. georefernzierte Rasterdaten, also ein Pixelbild, das entzert und verortet ist.
Luftbilder sind als Dateien verfügbar oder als Webservices.
Verbreitete Dateiformate sind JPG, PNG und TIFF.
Bei den Webservices haben sich die Standards '`Web Map Service`' (WMS) und '`Web Map Tiling Service`' (WMTS/XYZ/TMS) etabliert.
Einige Eigenschaften dieser Formate und Webservices werden hier noch erläutert.

Typische Probleme dabei sind, dass man erstens nicht weiss, wo Luftbilder zu finden sind, und zweitens,
wie man einen kleinen Ausschnitt davon herunterladen kann, und drittens wie dabei gleichzeitig das Koordinatensystem umtransformiert wird.

Dieser Beitrag möchte diese Probleme lösen mit dem Ziel eine lokale, georeferenzierte Rasterdatei zu erhalten im Format GeoTIFF,
PNG oder JPG mit den richtigen Sidecar-Dateien und im passenden Koordinatensystem.

.Das Dokument ist wie folgt aufgebaut

. (Diese) Einführung
. <<_2_problem_und_lösung_in_kürze, Problem und Lösung in Kürze>>
. <<_3_technische_informationen, Technische Informationen>>
. <<_4_ein_beispiel_bodenkarte_kt_zh, Ein Beispiel "`Bodenkarte Kt. ZH`">>
. <<_5_wie_auf_rasterdaten_zugreifen_und_einen_ausschnitt_davon_herunterladen, Wie auf Rasterdaten zugreifen und einen Ausschnitt davon herunterladen?>>
. <<_6_weitere_lösungsvarianten_mit_qgis, Weitere Lösungsvarianten mit QGIS>>
. <<_7_wie_rasterdaten_finden, Wie Rasterdaten finden?>>

wer sich schon etwas in Geoinformations-Technologien auskennt (oder die Details wenig interessiert),
der kann das Kapitel <<_3_technische_informationen, "`3. Technische Informationen`">> auslassen und direkt zum Kapitel <<_4_ein_beispiel_bodenkarte_kt_zh, "`4. Ein Beispiel...`">> springen.

Folgende Software muss vorbereitet sein für dieses Tutorial: QGIS 3, ein Webbrowser.
Wenn nötig muss QGIS (aktuell ist Version 3.22, mind. QGIS 3.16 LTR) installiert werden

TIP: Installation von QGIS 3: Öffnen Sie diesen Link https://www.qgis.org/de/site/forusers/download.html#[qgis.org]
und wählen Sie das passende Betriebssystem aus (z.B. Windows 64bit).
Eine Setup-Datei namens osgeo4w-setup.exe wird heruntergeladen und kann geöffnet werden.
Nach dem Ausführen der menu:Datei[Express Installation > QGIS].

==  Problem und Lösung in Kürze

Die Ausgangslage ist, dass ein grosses Rasterbild - z.B. von der Bodenkarte Kt. ZH - im GeoTIFF-Format oder als Webservice (WMS/WMTS/XYZ/TMS) exisiert.
Gesucht ist ein kleinerer Ausschnitt davon - z.B. ein Quartier - und zwar entweder wieder als GeoTIFF oder aber als PNG- oder JPG-Datei.
Ein weiteres mögliches Problem bei diesem Prozess ist, dass auch noch eine Koordinatensystem-Transformation durchgeführt werden muss.
Bei PNG und JPG ist zwingend noch die Erzeugung einer zusätzlichen Datei, eines sogenannten “World-File”, notwendig.

QGIS unterstützt diesen Prozess und nachfolgend sind die Schritte kurz beschrieben.

Lexocad erwartet ein PNG/JPG und es wird davon ausgegangen, dass das Rasterbild im Schweizer Koordinatenreferenzsystem (CH/LV95) ist und also nicht umtransformiert werden muss.
Zudem erwartet Lexocad anstelle eines World-Files eine Sidecar-Datei (PNGL/JPGL).
Das QGIS-Plugin ‘Raster Cutter’ ermöglicht das in einem Schritt.

TIP: Sie möchten wissen, wo man frei zugängliche Rasterdaten findet?
Im <<_4_ein_beispiel_bodenkarte_kt_zh, Kapitel 4>> wird ein Beispiel durchgespielt und im <<_7_wie_rasterdaten_finden, Kapitel 7>> steht mehr dazu - auch zu weiteren QGIS-Plugins.

=== A. Vorbereitung:

. QGIS starten und "`Neues Projekt`" eröffnen.
. Evtl. Basiskarte laden (z.B. OpenStreetMap oder MapGeoAdmin).
. Das QGIS-Projekt auf das Schweizer Koordinatenreferenzsystem CH/LV95 (EPSG:2056)
. Rasterdatei (Datenquelle) d.h. den "`Input`" bestimmen (GeoTIFF, WMS, WMTS/XYZ).
. Auf Ausschnitt zoomen. Die Ausdehnung (en: extent) festlegen.

=== B.1 ... für Smarte (und Lexocad-Anwender) mit dem QGIS-Plugin '`Raster Cutter`':

Die Lösungsschritte mit dem QGIS-Plugin '`Raster Cutter`' inklusive Koordinatenreferenzsystem-Transformation und inklusive Lexocad siehe im <<_bedienungsanleitung, Kapitel 5.2 unten>>.

[TIP]
====
Mit QGIS-Plugin `Raster Cutter` kann ein Ausschnitt auc als GeoTiff eportiert werden. +
Mehr hierzu ebenfalls im <<_bedienungsanleitung, Kapitel 5.2>>.
====

=== B.2 ... für PNG-/JPG-Export inkl World-File:

. `Raster auf Ausdehnung zuschneiden` (en: "`Clip Raster by Extent`") wählen und Resultat als temporärer Layer (GeoTIFF) anzeigen.
. Falls ein WMS-Layer verwendet wird, muss dieser als lokale Datei abgespeichert werden.
Falls der WMS-Layer nicht als lokale Datei abgespeichert wird, kann kein Ausschnitt mit `Raster auf Ausdehnung zuschneiden` erstellt werden.
. Der Ausschnitt in QGIS kann nun gespeichert werden unter menu:Projekt[Import/Export > Karte als Bild speichern...]
. Fertig (Laden und Betrachten mit QGIS)

=== B.3 ... für GeoTIFF-Export:

. Unter menu:Raster[Extraktion > Raster auf Ausdehnung zuschneiden] auswählen.
. Eine der Methoden unter Ausdehnung zuschneiden auswählen.
. Mit gültigen Parametern kann der Ausschnitt erstellt werden und das Resultat ("`Output`") als GeoTIFF gespeichert werden.
. Fertig (Betrachten mit QGIS)

TIP: Bei der Wahl eines Dateinamens immer darauf achten, dass er sinnvoll und selbstdokumentierend ist.
Einen Ausschnitt des Beispiels Bodenkarte Kanton Zürich (vgl. unten)
über die Parzellen 1000 und 1001 könnte man z.B. `Bodenkarte_KtZH_Parz_1000_1001.tif` nennen.
Namen wie `ausschnitt.tif`, `export.tif` oder `extent.tif` sind nichtssagend.
Ebenso werden Leerzeichen mit “_” dargestellt um Probleme in der Zukunft mit dem File zu verhindern.

.Bodenkarte Kt. Zürich (grösserer Ausschnitt)
image::bodenkarte_kt_zh_(groesserer_ausschnitt).png[]

==  Technische Informationen

Beim Umgang mit Rasterbildern in GIS muss man einige Dinge beachten.

=== Dateiformate JPG, PNG und GeoTIFF

Bei Dateien muss man das Format kennen.
Jedes Format hat seine Eigenschaften.
Wir konzentrieren uns hier auf die Bildformate JPG, PNG und TIF.
Der Vorteil von JPG ist u.a. die kleinere Dateigrösse im Vergleich zu PNG und TIF.
Der Nachteil gegenüber den beiden ist, dass das JPG-Format verlustbehaftet ist,
d.h. dass die Pixelwerte leicht verändert werden (wegen der Kompression) und dass JPG keine Transparenz kennt.

GeoTIFF ist ein TIF (oder .TIFF) welches ein Format zur Speicherung von Bilddaten ist.
Speziell am GeoTIFF ist, dass es eine verlustfreie Speicherung zulässt und Georeferenzierungs-Informationen in die Datei eingebettet hat.
Aus diesem Grund eignet es sich sehr gut für die Verarbeitung von geographischen Daten.

TIP: Hier eine Empfehlung für die Wahl des Raster-Dateiformats: Wenn das System GeoTIFF unterstützt, dann GeoTIFF - ansonsten PNG

=== Webservices WMS und WMTS/XYZ/TMS

.Zuerst zu den Abkürzungen

* WMS steht für "`Web Map Service`" und ist eine international genormte Schnittstelle zum Abrufen von Auszügen aus einer Karte über das Internet.
* WMTS steht für "`Web Map Tiling Service`" und ist eine international genormte Schnittstelle zum Abrufen von Auszügen einer Karte über das Internet mittels Kacheln.
* XYZ bzw. TMS (TMS: Tiles Map Service) ist ähnlich wie WMTS eine Schnittstelle zum Abrufen von Auszügen einer Karte über das Internet mittels Kacheln.
XYZ/TMS wurde von Google Maps und Open Source-Software bzw. OpenStreetMap geprägt.

Wenn man Webservices ansprechen will, braucht es eine Internet-Verbindung und man muss den Weblink kennen.
Wie ein WMS/WMTS und ein XYZ/TMS Service auf QGIS werden kann wird Ihnen im Abschnitt <<_4_ein_beispiel_bodenkarte_kt_zh, "`Bespiel der Bodenkarte des KT.ZH`">>  erklärt.

WMS liefert ein einziges Bild zurück, während WMTS/XYZ/TMS Kacheln (engl. tiles) zurückliefern.
Mehr zu WMS, WMTS und XYZ/TMS gibt es https://giswiki.hsr.ch/WMS[hier auf dem GISWIKI].

Basiskarten - wie zum Beispiel auch im Kapitel <<_basiskarten, "`Basiskarten`">> erwähnte Swisstopo-Luftbild - können nicht nur als Hintergrund sondern auch als Input-Layer verwendet werden.

TIP: Man beachte, dass QGIS einen einzigen Dialog für das Laden von WMS/WMTS-Layer bereitstellt und einen seperaten Dialog für XYZ/TMS-Layer.
WMS/WMTS-Services sind beide von OGC normiert worden.
Technischer ähnlicher zueinander sind jedoch eigentlich WMTS und XYZ/TMS, da diese mit Kacheln arbeiten.
Hier gibt es eine gute https://www.stadt-zuerich.ch/portal/de/index/ogd/werkstatt/wms_wmts/wmts_in_qgis.html[Einführung in die Einbindung von WMS/WMTS] (Stadt Zürich)

=== Begrenzungsgeometrie, Auflösung, Massstab

.Zwischen folgenden besteht ein direkter Zusammenhang

* Begrenzungsgeometrie (Bounding Box am Bildschirm Latitude/Longitude-Grad oder in CH-Koordinatensystem-Meter)
* Auflösung am Boden, d.h. Pixel-Grösse (v.a. in Meter)
* Massstabszahl (z.B. 1:5'000)

Der Massstab (en: scale) ist das Verhältnis zwischen der angezeigten Grösse und der tatsächlichen Grösse in der Realität.

Die Auflösung (en: resolution) ist die Grösse einer Gitterzelle auf dem Boden.
Eine hohe Auflösung bedeutet kleine Rasterzellen, mehr Rasterzellen und eine grössere Dateigröße.
Ein guter Wert für die Gitterauflösung ist: Auflösung = Massstabsfaktor / 2’000 (Hengl 2006).
Ein Massstab von 1:2’500 würde also eine Auflösung von 1.25 Meter bedeuten (2500/2000).
Wenn hochauflösende Daten zur Verfügung stehen, kann eine gröbere Auflösung verwendet werden, was eine Änderung der Zellengrösse bedeutet.

Der Prozess der Änderung der Zellengrösse wird als Resampling bezeichnet.
Er beinhaltet einen Resampling-Algorithmus.
Bei kategorischen Daten - wie z.B. der Bodenbedeckung - wählen Sie Nearest Neighbor (Standardeinstellung); dadurch wird der Z-Wert der Eingabezellen nicht verändert.
Für kontinuierliche Daten - wie z.B. Gelände, Wetter - Bilinear oder Cubic (besser aber langsamer), die beide interpolieren.

Die Pixelgrösse des Bildschirms oder der Daten sind nicht direkt relevant für den Massstab.
Der WMS-Standard z.B. geht von einer Pixelgrösse von 0.28mm (= 1 DPI) aus.
Für Fortgeschrittene gibt es https://github.com/qgis/QGIS/issues/15674#issuecomment-495660784[hier] weitere Übungen zu DPI/RESOLUTION im Zusammenhang mit WMS

TIP: Im QGIS-Menü menu:View[Zoom to Native Resolution] zoomt die Karte auf den Massstab mit der besten Auflösung für diesen Layer.

=== Koordinaten-Referenz-Systeme (KRS)

Das Koordinaten-Referenz-System (KRS, englisch: "`Coordinate Reference System`", manchmal auch Bezugssystem genannt) ist oft ein Stolperstein bei der Verwendung von Rasterbildern.
KRS wurden standardisiert (siehe https://epsg.io) und erhilten vier- bis fünfstellige Nummern, z.B. EPSG:2056 für das aktuelle KRS der Schweiz.
Die Transformation von einem System ins andere ist bei Rasterbilder aufwändig - jedenfalls aufwändiger als bei Vektordaten - und viele Systeme unterstützen das nicht.
Lexocad beispielsweise erwartet fest EPSG:2056

Das KRS an sich beinhaltet zu viel Inhalt um komplett in diesem Übungsblatt abzudecken.
Was jedoch für Rasterdaten in QGIS und KRS zusammen wichtig ist, ist, dass wenn man mehrere Datensätze verwendet werden, diese das gleiche KRS haben müssen.
Bei der Verwndung von zwei verschiedenen KRS stimmen die Daten der verschiedenen Datensätzen nicht mehr miteinander überein.
(Falls noch nicht klar ist, wofür das KRS steht und warum es wichtig ist, können Sie sich über
https://docs.qgis.org/3.16/en/docs/gentle_gis_introduction/coordinate_reference_systems.html[diesen Link] informieren)

Wenn man Datensätze verwendet, die spezifisch z.B. aus der Schweiz stammen,
muss das KRS umkonvertiert und/oder umtransformiert werden und zwar in das KRS LV95 2056.
Im Allgemeinen ist das KRS Webmercator 3857 der Default.

In QGIS kann das KRS LV95 2056 unter menu:Projekt[Eigenschaften > KRS] gewählt werden.
Da es viele KRS gibt, sind nicht alle in QGIS vorinstalliert.
Unter menu:Projekt[Eigenschaften > KBS] können vordefinierte KRS nachgeladen werden.
In der Vorschau sieht man auch auf welchen Teil der Welt sich das KBS bezieht.
Wenn man unter Layer-Eigenschaften geht, kann das KRS der einzelnen Layers unter Quelle ebenfalls angepasst werden.

TIP: Typische KRS für die Schweiz sind 2056 (CH1903+/LV95) und veraltet(!) 21781 (CH1903/LV03).
Dazu kommen 4326 (WGS84 lat/lon) und EPSG:3857 (Web-Mercator).

==== Rasterbilder georeferenzieren mit World-File (JGW, PGW, TFW, GeoTIFF)

Rasterbilder müssen georeferenziert sein damit eine Sofware wie QGIS "`weiss`", an welcher Koordinate ein Pixel liegt.
Die Bilddateien JPG, PNG und TIF alleine haben keine Angaben zur Georeferenzierung.
Daher wird eine sogenannte '`Sidecar-Datei`' (Filialdatei) benötigt.

Ein '`World-File`' ist so eine kleine zusätzliche Sidecar-Datei.
Sie erhält Georeferenzdaten in Textform.
Es ist eine Sidecar-Datei, die von der Firma ESRI mit ihrer Software (ArcGIS, ArcView) als Ergänzung für einfache Bildformate eingeführt wurde.
Die Dateinamenserweiterung leitet sich vom Format und lautet beispielsweise .JGW für JPG, .PGW für PNG oder .TFW für TIFF-Bilddateien.

TIP: Bei JPG- und PNG-Dateien kann das World-File - z.B. .JGW und
.PGW - auch nachträglich erzeugt werden, gegeben diese liegen in QGIS richtig georeferenziert vor.
Dabei in QGIS den Layer Selektieren und im Raster-Menü "`Extract Projection`" aufrufen!
Der Name dieser Funktion ist etwas verwirrend, denn die Sidecar-Dateien
.PRJ für die Projektion (d.h. das KRS) kann zwar auch erzeugt werden; doch das ist optional.

Eine GeoTIFF-Datei, welche die Georeferenzierung in der Datei selber eingebaut hat.
Sie enthält viel mehr Informationen und Daten als die oben erwähnte TFW-Sidecar-Datei.

IMPORTANT: Das KRS fehlt in den genannten Sidecar-Dateien!
D.h. die Rasterdatei muss z.B. in QGIS "`von Hand`" kontrolliert und ggf. im QGIS-Projekt gesetzt werden.
Nach dem öffnen eines Projektes in QGIS kann unter menu:Projekt[Einstellungen > KBS] das gewollte KBS gesetzt werden.

=== Rasterbilder georeferenzieren für Lexocad (JPGL, PNGL, GeoTIFF)

Für Lexocad ist zusäzlich zu JPG die Sidecar-Datei JPGL,
bzw. zu PNG die Sidecar-Datei PNGL erforderlich, also z.B. bodenkarte.jpg und bodenkarte.jpgl.
Diese Dateien beinhalten Georeferenzdaten des Bildes - analog dem World-File.
Diese zusätzlichen Sidecar-Dateien sind zwingend erforderlich, um die Dateien in Lexocad verwenden zu können.
Dann gibt es Lexocad noch die Möglichkeit eine lokale GeoTIFF-Datei einzulesen.
Diese wird dann als Punktwolke interpretiert.
Dabei wird der Raster-Pixel-Wert als Höhenangabe verwendet.

[TIP]
.für Lexocad-Benutzer:
====
* Lexocad benötigt eine eigene Sidecar-Datei für JPG und PNG: Siehe unten.
* Wenn man eine Datei mit einer Rasterdatei mit World-File hat und diese in Lexocad verwenden will,
dann kann man dieses in QGIS laden und dann mit dem Plugin speichern.
D.h. man verfährt wie einen Ausschnitt mit QGIS speichern (siehe unten).
====

==  Ein Beispiel Bodenkarte Kt. ZH

Nachdem das Technische oben erläutert wurde, zeigen wir jetzt, wie ein Rasterbild in QGIS betrachtet werden kann.
Als Beispiel verwenden wir diesen WMS (Basis-URL) der "`Bodenkarte der Landwirtschaftsflächen des Kantons Zrüich`" (kurz: Bodenkarte Kt.ZH).


.Folgende Schritte genau einhalten

. QGIS starten und neues Projekt eröffnen
. Die Basiskarte "`OpenStreetMap Standard`" laden und zwar im Browser-Tab von QGIS unter "`XYZ TILES`".
. Im Karten-Fenster rechts unten auf das Koordinatensystem (CRS) klicken und im Dialog "`EPSG:2056`" wählen.
. Im Karten-Fenster in die Schweiz auf einen Massstab von ca. 1:5000 hineinzoomen.
. Im Browser-Tab von QGIS unter menu:WMS/WMTS[Bodenkarte KT. ZH (WMS)] anwählenund dann auf "`Kartiereinheit`" klicken.
Im Tab "`Layers`" kontrollieren.
. Falls "`Bodenkarte KT. ZH (WMS)`" nicht vorhanden, diese einmalig mit Rechtsklick auf "`New Connection`" klicken.
Im Pop-Up den WMS Layer benennen (z.B. "`Bodenkarte Kt.ZH (WMS)`"), und die Basis-URL
`+http://wms.zh.ch/OGDBoKaZH+` einfügen, dann Layer_Name '`Kartiereinheit`' wählen
(Hinweis: diese Basis-URL ist ein Webservice, d.h. sie ist _nicht_ zum draufklicken und _nicht_ für den Webbrowser bestimmt).
Mit "`OK`" bestätigen wir.
(Weiterer Hinweis: Falls der Massstab nicht 1:10000 oder grösser 1:25000 ist, erscheint keine Karte!)

.Bodenkarte Kt. ZH (Braun- und Grüntöne) mit Basiskarte "`OpenStreetMap Standard`" im Hintergrund (graue Farben)
image::bodenkarte_kt_zh(braun-_und_gruentoene)_mit_basiskarte.png[]

==  Wie auf Rasterdaten zugreifen und einen Ausschnitt davon herunterladen?

Wer eine Rasterdatei oder einen Webservice gefunden hat, möchte nun auf diese zugreifen und einen Ausschnitt davon herunterladen.
Für diesen Zweck ist QGIS gut geeignet und bietet viele verschiedene Möglichkeiten.

In den folgenden Kapiteln werden zwei Varianten vorgestellt, um mit QGIS einen Auschnitt zu erstellen und herunterzuladen.
Danach sind “Weitere Lösungsvarianten mit QGIS” zusammengestellt.

=== Rasterdaten-Ausschnitte herunterladen mit QGIS out-of-the-box

QGIS bietet die Möglichkeit, Rasterdaten als Webservice (WMS/WMTS/XYZ/GeoTIFF) oder Datei (GeoTIFF) zu öffnen und zu visualisieren.
QGIS bietet von Grund auf schon viele Funktionen an, einen Ausschnitt davon zu speichern.
Diese Lösungsvarianten werden unter genauer vorgestellt.

Bei allen Lösungsvarianten basierend auf den Grundfunktionen von QGIS gibt es jedoch Wichtige Einschränkungen.
Im nächsten Kapitel wird darum ein neues QGIS-Plugin beschrieben,
das diese Einschränkungen nicht hat und - nebst dem World-File - zusätzlich die entsprechenden PNGL/JPGL-Datei erzeugt.

Im Kapitel "`Weitere Lösungsvarianten mit QGIS`" werden die in QGIS vorhandenen Lösungsmöglichkeiten erläutert.

TIP: Für QGIS findet man eine https://docs.qgis.org/3.16/de/docs/user_manual/working_with_raster/index.html[offizielle Dokumentation]
(v3.16 en), welche auch die Arbeit mit Rasterdaten beinhaltet.
Ebenfalls findet man auf https://openschoolmaps.ch/pages/materialien.html[OpenSchoolMaps] einige Aufgaben, die den Umgang mit QGIS und Rasterdaten beinhalten.

=== Rasterdaten-Ausschnitte herunterladen mit dem QGIS-Plugin Raster Cutter

Das Ziel dieses neuen Plugins ist es, in einem einzigen Schritt aus einer grossen GeoTIFF-Datei oder aus WMS/WMTS/XYZ-Services einen Ausschnitt
(engl. cut, clip, extract) zu erstellen, und diesen Ausschnitt lokal zur Weiterverarbeitung zu speichern als
GeoTIFF-, PNG- oder JPG-Datei und zwar mit passendem Koordinatenreferenzsystem und inkl. Erzeugen eines  "`World-File`" (zusätzlich auch für LexoCAD).

TIP: QGIS-Plugins können unter dem Menü menu:Plugins[Manage and Install Plugins] aktiviert oder heruntergeladen werden (ggf. "`Experimentelle Plugins zulassen`").

==== Bedienungsanleitung

.Dies sind die Lösungsschritte mit dem QGIS-Plugin '`Raster Cutter`' inklusive Koordinatenreferenzsystem-Transformation und inklusive Lexocad

. QGIS Starten - ggf. QGIS-Plugin '`Raster Cutter`' installieren und "`Neues Projekt`" eröffnen
. Das QGIS-Projekt auf das CRS "`EPSG:2056`" (= Schweizer Koordinatenreferenzsystem CH/LV95)
. Gegebenenfalls Hintergrund-/Basis-Karte laden (z.B. OpenStreetMap oder MapGeoAdmin).
Vergewissern, dass das CRS immer noch "`EPSG:2056`" ist.
. Rasterdate/Datenquelle (= Input-Layer) laden (WMS, WMTS/XYZ/TMS, GeoTIFF) und auf den gewünschten Ausschnitt zoomen.
. Den Dialog des Plugins '`Raster Cutter`' öffnen und dort die nötigsten Parameter bestimmen:
+
--
. Input-Layerwählen (falls nicht schon selektiert)
. Die Ausdehnung (en: extent) festlegen indem man z.B. auf "`Map Canvas Extent`" klickt.
. Pfad und Name des Outputs festlegen sowie das Ausgabeformat (GeoTIFF, PNG oder JPG).
. Falls gewünscht, die "`Create LexoCAD`" Checkbox ankreuzeln, um LexoCAD Georeferenzdateien zu generieren.
--
+
. Gegebenenfalls weitere Parameter setzen (für Fortgeschrittene): CRS und Output resolution.
. Fertig (Laden und Betrachten mit QGIS oder Lexocad)

.Dialog des neuen QGIS-Plugins '`Raster Cutter`'
image::dialog_raster_cutter.png[]

==== Über das QGIS-Plugin Raster Cutter

Bei allen Lösungsvarianten basierend auf den Grundfunktionen von QGIS gibt es wichtige Einschränkungen.

 * Bei keiner Lösungsvariante ausser menu:Raster[Translate (Convert)] gibt es die Möglichkeit, als PNG oder JPG zu speichern.
 * menu:Raster[Translate (Convert)] kann zwar PNG oder JPG speichern, aber es kann kein Ausschnitt angegeben werden.
 * Bei den anderen Lösungsvarianten kann keine Koordinaten-Transformation - d.h. kein Wechsel des KRS ("`Clip Raster by Extent`") - angegeben werden.

D.h. dieses Plugin ist sozusagen eine Kombination von drei GDAL-basierten QGIS-Processing-Funktionen.

.Limitationen des Plugins bzw. Weiterentwicklungsmöglichkeiten

* GeoTIFF-Dateien müssen als Ganzes in Memory passen, d.h. sind Main Memory-limitiert - wie alle anderen Funktionen von QGIS auch.
* Umgang mit Transparenz und Z-Values (Überschreiben, NODATA, Klassieren).

TIP: Geben Sie Feedback zum QGIS-Plugin '`Raster Cutter`' - wie auch zu dieser Dokumentation!
https://github.com/geometalab/qgis-raster-cutter[GitHub] (aktuell Version 0.5)

==  Weitere Lösungsvarianten mit QGIS

Folgende Lösungsvarianten für das Ausschneiden und Herunterladen/Exportieren gibt es in QGIS (am Beispiel Bodenkarte Kt. ZH als GeoTIFF oder WMS).
Im folgenden Abschnitt werden 4 Lösungsvarianten erwähnt.
Wie die einzelnen Lösungsvarianten funktionieren und was ihre Vor- und Nachteile sind, wird weiter unten erklärt.

. Rasterdatei mit "`Karte als Bild Speichern`" exportieren.
. Rasterdatei mit "`Raster auf Ausdehnung zuschneiden`".
. Rasterdatei mit "`Rasterlayer speichern als...`" (entwder via menu:Menu[Layer] oder menu:Layer-Menu[Export])
. Ausschnitt herunterladen für Fortgeschrittene.

[NOTE]
.Weitere Tutorials gibt es hier:
====
* Tutorial "`Erstellen und zuschneiden eines Raster-Mosaiks`" von Ujaval Gandhi (2021? deutsch. Benutzt "`Clip Raster by Mask Layer`"):
https://www.qgistutorials.com/de/docs/3/raster_mosaicing_and_clipping.html[]

* Video-Tutorial "`QGIS 3: How to Clip Raster Image in QGIS`" von Benutzer "`miXcel`"
(17.05.2020 englisch. Benutzt "`Clip Raster by Mask Layer, Export`"):
https://www.youtube.com/watch?v=I69n2fah1GY[]

* Video-Tutorial "`Clip a Raster Layer in QGIS (three different ways)`" von Benutzer "`Open Source Options`"
(30.03.2021, englisch. Benutzt "`Clip Raster by Extent`" und "`Clip Raster by Mask Layer`"):
https://www.youtube.com/watch?v=hPSIW1W3XjY[]
====

=== 1. Rasterdatei mit "`Karte als Bild speichern`" (en: "`Export als Image`")

PNG ist eines der bekanntesten Dateiformaten um Bilder zu speichern.
"`Karte als Bild speichern`" ist auf QGIS jedoch die einzige Möglichkeit eine Rasterdatei als PNG/JPG zu speichern.

Die im Beispiel verwendete Datei ist der Übersichtsplan Kanton ZH(WMS:
https://wms.zh.ch/upwms?SERVICE=WMS&VERSION=1.3.0&REQUEST=GetMap&BBOX=698522,232965,701777,235179&WIDTH=10000&HEIGHT=7000&FORMAT=image/png&LAYERS=upwms&CRS=EPSG:21781[])

Im ersten Schritt erstellen wir eine Verbindung mit dem WMS Service mit der oben erwähnten URL
(Hilfe zu WMS Verbindung erstellen ist im Kapitel Webservices WMS und WMTS/XYZ erhältlich).
Nach dem erfolgreichen Erstellen einer Verbindung kann der Layer mithilfe des WMS Service eingefügt werden.

Die im QGIS angezeigten Layer können mit dem Mausrad vergrössert und verkleinert werden.
Wenn man mit dem Auschnitt, der im Kartenfenster angezeigt wird, zufrieden ist, kann über
menu:Projekt[Import/Export > Karte als Bild speichern] das Format und der Dateiname bestimmt werden.

Als Resultat erhält man ein PNG der Kartenansicht von QGIS:

.PNG Ausschnitt des Übersichtsplans vom Kt.ZH
image::ausschnitt_des_uebersichtsplan_von_kt_zh.png[]

.Vorteile

* Mit wenigen Schritten erhält man mit dieser Methode beretis ein Ergebnis
* Nur mit dieser Methode kann als PNG/JPG gespeichert werden

.Nachteile

* Nur der Teil der auf der Kartenansicht gezeigt ist wird gespeichert
* Schlecht anpassbar

=== 2. Rasterdatei mit "`Raster auf Ausdehnung zuschneiden`" (en: "`Clip Raster by Extent`")

"`Raster auf Ausdehnung zuschneiden`" ist eine Möglichkeit, in QGIS einen Ausschnitt einer Rasterdatei zu erstellen.
Wie der Name andeutet, erstellt diese Funktion einen Ausschnitt des Rasterbild zu einem gegebenen Ausschnitt

Im folgenden zeigen wir Ihnen, wie man einen Ausschnitt von einem grossen GeoTIFF zuschneidet.
Im Beispiel wird die Bodenkarte des Kt. ZH verwendet, welche als Geotiff(share link) oder als WMS Service
(https://wms.zh.ch/upwms?SERVICE=WMS&VERSION=1.3.0&REQUEST=GetMap&BBOX=698522,232965,701777,235179&WIDTH=10000&HEIGHT=7000&FORMAT=image/png&LAYERS=upwms&CRS=EPSG:21781[]) geladen werden kann.


Wenn die Datei über den shared Link oder den WMS Service importiert wurde, sollten im Layer Fenster der Uebersichtsplan Kanton ZH erscheinen.

Die Funktion "`Clip Raster by Extent`" kann bei QGIS im Menü gefunden werden unter menu:Raster[Extraction > Raster auf Ausdehnung zuschneiden].
Der dafür verwendete Algorithmus basiert auf GDAL.

Es öffnet sich ein Dialogfenster, in welchem Sie den Layer auswählen können, von dem Sie einen Ausschnitt erstellen wollen.

.Raster auf Ausdehnung zuschneiden Einstellungen
image::raster_auf_ausdehnung_zuschneiden_einstellungen.png[]

Um einen Ausschnitt zu erstellen, stehen verschiedene Möglichkeiten zur Verfügung.
Diese sind unten einzeln erläutert und können unter Ausdehnung zuschneiden aufgefunden werden.
An dieser Stelle empfehlen wir, vor dem Starten der Funktion den auszuschneidenden Layer im Layers-Panel anzuwählen und auf den gewünschten Kartenausschnitt zoomen.

Was bei dieser Lösung fehlt, ist die einfache Angabe eines Ausgabeformats (wie PNG und JPG), das nicht GeoTIFF ist.

Das geht einerseits über "`Zusätzliche Komandozeilen-Parameter`" (vgl. Hinweis unten, der sich an Fortgeschrittene richtet)
oder über einen zweiten Verarbeitungs-Schritt (vgl. das Kapitel "`Zusammenfassung`" oben).
Ansonsten hilft evtl. ein Plugin (das es aber noch nicht gibt).

TIP: Das Ausgabeformat PNG kann beim Dialog "`Raster auf Ausdehnung zuschneiden`" unter "`Zusätzliche Komandozeilen-Parameter`" mit "`-of PNG`" angegeben werden.
Dabei ist bei PNG zu beachten, dass der "`Output Data Typ`" Uint16 oder Byte ist.

==== Einen Ausschnitt wählen
Im Dialog der sich bei der Auswahl von "`Raster auf Ausdehnung zuschneiden`" öffnet, kann unter Ausdehnung zuschneiden das Dropdown geöffnet werden.

.Folgende drei Optionen, um einen Ausschnitt zu erstellen, werden nun angezeigt

. Kartenausschnitt verwenden (en: Use Map Canvas Extent)
. Aus Layer berechnen (en: Calculate From Layer)
. Auf Kartenansicht zeichnen (en: Draw on Canvas)

==== Kartenausschnitt verwenden (en: Use Map Canvas Extent)

Wenn sie "`Use Map Canvas Extent`" verwenden, wird der Ausschnitt auf der QGIS-Kartenansicht ausgeschnitten.
Die Grösse kann durch hinein- oder herauszoomen auf der QGIS-Anzeige verändert werden.
Mit einem erneuten Klick auf "`Use Map Canvas Extent`" werden die Ausschnittswerte aktualisiert.
Unter Fortgeschrittene Parameter können Sie den Speicherort auswählen.
Mit Starten wird der Algorithmus von GDAL ausgeführt und die Datei gespeichert.

Vorteile:

* Schnell auffindbar
* Einfach bedienbar
* Der Ausschnitt der Layer kann angepasst werden

Nachteile:

* Der Zoom muss manuell auf QGIS Karte angepasst werden

==== Ausschnitt aus Layer berechnen (en: Calculate From Layer)

Wenn Sie "`Calculate From Layer`" anklicken, wird Ihnen eine Liste von Layers angezeigt, welche in QGIS momentan geöffnet sind.
Wählen Sie die Layer aus, von welchen Sie einen Ausschnitt erstellen wolen.
Nach Bestätigung wird automatisch das Minimum und Maximum von x und y der ausgewählten Layer eingefügt.
Legen Sie als letzter Schritt nun noch einen Speicherort fest und mit Start wird der GDAL Algorithmus gestartet.

.Vorteile

* Schnell auffindbar
* Einfach bedienbar

.Nachteile

* Die maximal unterstützte Bild Dimension ist 65500x65500 Pixels

==== Ausschnitt auf Kartenansicht zeichnen (en: Draw on Canvas)

Mit "`Draw on Canvas`" kann ein benutzerdefinierter Bereich ausgewählt werden.
Mit Klick auf "`Draw on Canvas`" werden Sie auf die Kartenanzeige geleitet.
Dort können Sie mit dem Fadenkreuz und Rechtsklick einen benutzerdefinierten Bereich auswählen.
Nach dem Festlegen des Bereichs, werden sie zurück auf das Dialogfenster geleitet, in welchem die Daten automatisch übertragen werden.
Wenn Sie den Speicherort festgelegt haben und mit Ihrem ausgewählten Ausschnitt zufrieden sind, können sie den GDAL Algorithmus über "`Starten`" ausführen.

.Vorteile

* Schnell auffindbar
* Der Ausschnitt kann mit dem Fadenkreuz bestimmt werden
* Mit dem Mausrad kann der Zoom angepasst werden

.Nachteile

* Das Anpassen kann nur mit dem Fadenkreuz gemacht werden.

=== 3. Rasterdatei mit "`Speichern als`" (en: "`Safe Raster Layer As`")

QGIS bietet die Möglichkeit einen Layer zu speichern.
Dazu kann der gewünschte Layer mit menu:Rechtsklick ausgewählt werden[Export > Speichern als...]

In diesem Beispiel verwenden wir weiter die Bodenkarte des Kt.ZH(Gleiche WMS Adresse wie im anderen Bsp.)

.Speichern als Location
image::speichern_als_location.png[]

Nun öffnet sich ein Fenster in welchem man Einstellungen wie das Wunschformat, die Ausdehnung Extent und das KBS bestimmen kann.

.Speichern als Einstellungen
image::speichern_als_einstellungen.png[]

[CAUTION]
.Die folgenden Einstellungen müssen übernommen werden!:
====
* VRT erzeugen muss ausgeschaltet sein
* Der Dateiname muss ausgefüllt sein
* KRS nach Bedarf anpassen, damit der Datensatz nicht verschoben ist
* Die Auflösung muss wie im Bild übernommen werden (z.B. 1m)
====

Nun muss noch das GeoTIFF in PNG/JPG gewandelt werden.
Siehe "`... für Fortgeschrittene`"...

.Vorteile

* Schnell und einfach auffindbar
* Einstellungsmöglichkeiten wie Formatauswahl

.Nachteile

* Unter der Format-Wahl kann kein JPG oder PNG ausgewählt werden; nur GeoTIFF ist nutzbar.
Dafür ist ein komplett separater Schritt nötig.
* Modaler Dialog; es gibt auch keine zusätzlichen Kommandozeilen-Parameter (für Fortgeschrittene).
* Unklar, ob das die geeignete/beste Zoom-Stufe war

=== 4. Ausschnitt herunterladen für Fortgeschrittene

Man kann Rasterdateien mittels GDAL in dutzende Formate konvertieren.

Dazu Bietet sich entweder der Online-Konverter geoconverter.infs.ch an - oder aber GDAL.

GDAL wird mit QGIS mit installiert und lässt sich in der Kommandozeile aufrufen.
Wie das geht wird im OpenSchoolMaps-Arbeitsblatt (@TODO) gezeigt.

Wenn man zusätzlich noch das World-File erstellen will, muss beachtet werden, dass bei der Kommandozeile "`World-File=yes`" angegeben wird.

---

Weblink (URL) des Webservices direkt in QGIS eingeben - wenn man ihn weiss (z.B. vom Suchportal)

.Es folgende Orte bei denen QGIS Funktionalitäten stecken

. Eingebaut (z.B. Export Layer Definition File)
. Unter Verwendung von externen Libraries (z.B. GDAL/OGR)
. Als Plugin (vorkompiliert als C++ oder aber nachladbar in Python)
. Als Processing Tool (das seinsersets GDAL/OGR nutzt oder via Plugin installiert wird)

Ausserhalb QGIS gibt es die Möglichkeit, GDAL auf der Kommandozeile zu verwenden.
Dazu muss man GDAL kennen.
Einige QGIS-Dialoge helfen einem dabei.

Oder aber man lädt einen Ausschnitt eines WMS-Webservice direkt herunter.
Dazu sind genaue Kenntnisse des WMS nötig.

TIP: Das QGIS Plugin "`Bounding Box`" bietet gute Dienste zur Bestimmung des wichtiger WMS-Parameters "`Bounding Box`".
QGIS Plugin "`Bounding Box`": Nach dem Starten des Plugins, kann die Bounding Box des Anzeigefensters, welche aus (xmin, ymin, xmax, ymax) besteht, angezeigt werden.
Ebenfalls wird ein WMS "`GetMap Request`" für den vorhin ausgewählten Ausschnitt der Layer erstellt.

==  Wie Rasterdaten finden?

In der Schweiz bekannte Möglichkeiten zur Datenbeschaffung sind unter anderem der Kartenviewer MapGeoAdmin von Swisstopo oder kantonale Kartenviewer.

Dann gibt es noch das offizielle Suchportal opendata.swiss für offene Schweizer Daten oder Zürcher Portal GeoLion, die jedoch keine Kartenvorschau bieten.

.Folgende Lösungsvarianten, um WMS/WMTS/XYZ-Dienste zu finden empfehlen wir

* Suche in Kartenportalen
* Suche in QGIS (QMS Plugin)
* Suche für Fortgeschrittene

TIP: Rasterdaten und allgemein offene Geodaten im Internet zu finden ist immer noch im Wandel un mit einigem Aufwand und viel Erfahrung verbunden.
Wir vom Geometa Lab haben diese Erfahrung und bieten Kurse (siehe diese https://giswiki.hsr.ch/Agenda[Agenda]) sowie Unterstützung an (siehe Kontaktadresse zu diesem Dokument).

=== Häufig genutzte Karten-Webdienste in der Schweiz

* Swisstopo swissALTI (WMS) multidirektionales Relief: `+https://wms.geo.admin.ch/+`
* Swisstopo swissIMAGE (WMTS/XYZ): `+https://wmts.geo.admin.ch/1.0.0/ch.swisstopo.swissimage/default/current/3857/{z}/{x}/{y}.jpeg+` - siehe auch QMS https://qms.nextgis.com/geoservices/4683/[]
* Daten der Amtlichen Vermessung (AV) einzelner Kantone der Schweiz (WMS): via https://geodienste.ch/[geodienste.ch]
* OSM Swiss Style: siehe QMS bzw. as XYZ/TMS in QGIS vorinstalliert

=== Basiskarten

Typische Basiskarten sind das Luftbild, die Schweizer Karte von Swisstopo oder OpenStreetMap-Basiskarte.
Während der Bearbeitung liefert sie eine Orientierung bei der Geodaten-Erfassung.

Hier als Beispiel das Luftbild ("`swissIMAGE`") von Swisstopo als Webservice-Weblink im XYZ-Format (WMS/WMTS/XYZ):
`+https://wmts.geo.admin.ch/1.0.0/ch.swisstopo.swissimage/default/current/3857/{z}/{x}/{y}.jpeg+`.

TIP: Es gibt in QGIS-Plugin '`Swiss Geo Downloader`' mit dem man einfach Swisstopo-Karten herunterladen kann.
Und es gibt für Rasterdaten ein generelles QGIS-Plugin '`STAC API Browser`'.
QGIS Plugins lassen sich einfach aus QGIS heraus installieren.

=== Suche in Kartenportalen

Das Kartenportal MapGeoAdmin (https://map.geo.admin.ch) bietet eine interaktive Webseite, die auf der Webseite dargestellt und angepasst werden können.

Kartenviewer des Kantons Zürich https://maps.ch.ch/[] (siehe auch "`Datenquellen zur Schweiz`" unten) hat eine grosse Auswahl an Karten zu jeglichen Themen.
Diese können auf der Webseite angezeigt, und bei Bedarf ebenfalls als JPEG/World-File heruntergeladen werden.

=== Suche in QGIS

Suche von Geodaten bzw. Rasterdaten mit dem QGIS Plugin "`Quick Map Services`" (QMS) "`Search QMS`".
Nach dem Installieren des QMS Plugin, kann es in dem Werkzeugkasten aufgefunden werden: https://qms.nextgis.com/[].
QMS bietet mehrere  Möglichkeiten, Daten hinzuzufügen.
Die erste Möglichkeit ist, bereits vom Plugin vorinstallierte Services zu verwenden.
Die Mehrheit der Daten stammen von bekannten Anbietern wie z.B. NASA, Geofabrik, oder OpenStreetMap.

.Die oben erwähnte QMS-Website bietet nur einen einfachen Filter an. Bei der Eingabe von "`swiss`" erscheinen unter anderem

* Landeskarte farbig: "`SwissFederalGeoportal.NationalMapColor`"
* Landeskarte grau "`SwissFederalGeoportal.NationalMapGrey`"
* "`OSM.ch Swiss Style`"

Das QMS-Plugin in QGIS bietet jedoch auch die Möglichkeit, Filter by extent zu verwenden.
Bei dieser Methode wird der Kartenausschnitt, welcher im QGIS Anzeigefenster sichtbar ist, nach verfügbaren Servicen gefiltert.
Diese werden dann in 3 Kategorien eingeteilt('`works`', '`problematic`' und '`failed`').
Ebenfalls ist eine Vorschau des Service möglich und die Details können angezeigt werden.

=== Suche für Fortgeschrittene

. Suche auf opendata.swiss: https://opendata.swiss[]
. Google Dataset Search: https://datasetsearch.research.google.com/[]
. Google-Suche für Fortgeschrittene z.B. mit "`allinurl:service inurl:ch WMS`" und dann im Browser oder in QGIS eingeben.

.Stefan's ausgewählte Datensätze (tbc.)

* OpenEnergyData. https://github.com/SFOE/open_energy_data/blob/master/open_energy_data.md[] (vom BFE)
* GIS Kt. ZH: https://maps.zh.ch/[] => WMS der Bodenkarte der Landwirtschaftsflächen Kt. ZH
* Swisstopo ff.: https://www.swisstopo.admin.ch/de/geodata/images/ortho.html[] => WMST entnehmen
* Via Opendata.swiss: https://opendata.swiss/de/dataset/swissalti3d-reliefschattierung-multidirektionalswissALTI3D[] => multidirektionales Relief
* Die u.a. hier verwendeten Beispiele von Datenquellen sind: +
**{sp}WMS: Bodenkarte der Landwirtschaftsflächen Kt. ZH URL
http://wms.zh.ch/OGDBoKaZH[], Webbarte https://maps.zh.ch?topic=BoKaZH&scale=1780&x=2700008.49&y=1233063.84&srid=2056[] , Metadaten https://www.geolion.zh.ch/geodatensatz/show?gdsid=92[] ,
* WMS: ÖREB-Kataster (öffentlich-rechtliche Eigentumsbeschränkungen) Kt. ZH Metadaten https://www.geolion.zh.ch/geodatenservice/2028[]
* WMS: Digitales Terrainmodell (DTM) - 2017/2018 Kt. ZH Karte https://maps.zh.ch/s/k6d5p42f Metadaten https://www.geolion.zh.ch/geodatensatz/show?gdsid=520[]
* GeoTIFF: osmviews.tiff Welt
* Swisstopo WMS: https://wms.geo.admin.ch/[]
* Vereinfachte Bodennutzung
* Geotechnische Karte 200
* Hanglagen

=== Rasterdaten-Service gefunden - Wie nun (in QGIS) weiter?

Gegeben Sie haben einen Raster-Service - d.h. eine Basis-URL für einen WMS/WMTS/XYZ - gefunden, dann stellt sich als nächstes die Frage, wie man auf diesen in QGIS zugreift.
Dafür wurde u.a. das QGIS-Plugin '`Raster Cutter`' entwickelt.
Wie dieses funktioniert ist im <<_5_wie_auf_rasterdaten_zugreifen_und_einen_ausschnitt_davon_herunterladen, Kapitel 5 oben>> dokumentiert.
Doch es gibt immer noch einerseits natürlich die in QGIS eingebaute Funktionale, das bereits erwähnte QMS-Plugin sowie folgende Plugins, die nützlich sein können:

* Download von **Swisstopo- bzw. Swiss Geo Admin-Daten** mit dem QGIS-Plugin "`https://plugins.qgis.org/plugins/swissgeodownloader/[Swiss Geo Downloader]`".
* Download von **Satelitenbildern (v.a. NASA und ESA)** mit dem QGIS-Plugin "`STAC API Browser`"

== ANHANG: QGIS-Dialoge für Rasterdaten-Download

Hier zur Info die modalen Dialoge in QGIS der drei QGIS-Funktionen, die mit dem Erstellen von Ausschnitten aus Rasterbildern zu tun haben:

.Layer Export
image::layer_export.png[]

.Raster transformieren.
image::raster_transformieren.png[]

.Print-Layout als Bild speichern.
image::print-layout_als_bild_speichern.png[]

---
:imagesdir: ../../../bilder/
include::../../../snippets/kontakt_qgis.adoc[]

include::../../../snippets/license.adoc[]
